<?php

namespace App\Models\Admins\Repositories;

use App\Models\Admins\Admin;
use App\Repositories\BaseRepository;
use App\Models\Admins\Repositories\AdminRepositoryInterface;
use App\Models\Admins\Repositories\ViewData;

class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Admin $admin)
    {
        $this->model = $admin;
    }

    public function apiLogin($data)
    {

        if (!$token = auth('admin_api')->attempt(["username" => $data['username'], "password" => $data['password']])) 
        {

             return ["status" => false, "message" => "Invalid credentials", "data" => ""];
      }// send response
        
    return ["status" => true, "message" =>  "Logged in successfully", "data" => $token];

    }



}

