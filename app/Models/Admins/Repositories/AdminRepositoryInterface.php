<?php

namespace App\Models\Admins\Repositories;
use Illuminate\Http\Request;
 
interface AdminRepositoryInterface
{
    public function apiLogin($data);
}

