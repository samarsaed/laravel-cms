<?php

namespace App\Models\Operators\Repositories;

use App\Models\Operators\Operator;
use App\Repositories\BaseRepository;
use App\Models\Operators\Repositories\OperatorRepositoryInterface;
use App\Models\Operators\Repositories\ViewData;

class OperatorRepository extends BaseRepository implements OperatorRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Operator $operator)
    {
        $this->model = $operator;
    }
     
    public function apiLogin($data)
    {

         if (!$token = auth('operator_api')->attempt(["username" => $data['username'], "password" => $data['password']])) 
         {

             return ["status" => false, "message" => "Invalid credentials", "data" => ""];
         }// send response
        
       return ["status" => true, "message" =>  "Logged in successfully", "data" => $token];

    }


}

