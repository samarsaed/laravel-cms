<?php

namespace App\Models\Operators\Requests;
use Illuminate\Foundation\Http\FormRequest;

class OperatorLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string',
            'password' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'username' => __('api.username'),
            'password' => __('api.password'),
        ];
    }
}
