<?php

namespace App\Models\Operators\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreOperatorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'username' => 'required|string|unique:operators,username',
            'email' => 'nullable|email|unique:operators,email',
            'password' => 'required|string|min:6',
        ];

    }

    public function attributes()
    {
        return [
            'username' => __('api.username'),
            'email' => __('api.email'),
            'password' => __('api.password'),
        ];
    }
}
