<?php

namespace App\Models\EntityValues\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreEntityValueRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
           
            'entity_id' =>'required|exists:entities,id',
            'values' => 'required|array',
            
        ];

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.entityvalues.'),
        ];
    }
}
