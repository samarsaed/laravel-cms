<?php

namespace App\Models\EntityValues\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEntityValueRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';
        // rules

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.entityvalues.'),
        ];
    }
}
