<?php

namespace App\Models\EntityValues\Repositories;

use App\Models\EntityValues\EntityValue;
use App\Repositories\BaseRepository;
use App\Models\EntityValues\Repositories\EntityValueRepositoryInterface;
use App\Models\EntityValues\Repositories\ViewData;
use App\Models\Entities\Repositories\EntityRepositoryInterface;
use Illuminate\Support\Str;
class EntityValueRepository extends BaseRepository implements EntityValueRepositoryInterface
{
    use ViewData;
    private $entityRepository;

//    protected $with = [''];

	public function __construct(EntityValue $entityvalue,EntityRepositoryInterface $entityRepository)
    {
       
        $this->entityRepository = $entityRepository;
    
        $this->model = $entityvalue;
    }

    public function create(array $data)
    {
        $this->unsetClauses();

        $entity=$this->entityRepository->getById($data['entity_id']);
        if(!$entity->attributes()->count()){
            return ["status" => false, "message" => __("entity doesn't have attributes"),"data" => ''];
        }
            $ids = array_keys($data['values']);
          
          //validations  
          foreach($entity->attributes()->get() as $attribute)
          {
               $validation = json_decode($attribute->pivot->validations,JSON_UNESCAPED_UNICODE);
               if($validation[0]['is_required'] == 1 && !in_array($attribute->pivot->id,$ids)){
                return ["status" => false, "message" => $attribute->name." is required","data" => ''];
               }
               
               if($validation[0]['is_required'] == 0 && !in_array($attribute->pivot->id,$ids)){
                        $element[$attribute->name] = null;
               }
          }
          
          $record=[];
          foreach($data['values'] as $key=>$value)
          {
              
             $name = $entity->attributes()->wherePivot('id',$key)->first()->name;
             $element[$name] = $value;
          }
          $unid=Str::uuid();
          $element['unid'] = $unid;
          array_push($record, $element);
          $record = json_encode($record,JSON_UNESCAPED_UNICODE);

          $newRecord = $this->model->create([
           'entity_id' => $entity->id,
           'unid' =>  $unid,
           'record_values' => $record 
          ]);
        
          return ["status" => true, "message" => __("api.success"), "data" => $newRecord ];
    }

    public function apiGetEntityRecords($id)
    {
        $records = $this->model->where('entity_id',$id)->pluck('record_values')->toArray();
        $data=[];
        foreach($records as $record)
        {
            $record = json_decode($record,JSON_UNESCAPED_UNICODE);
            array_push($data, $record);
        }
        return ["status" => true, "message" => __("api.success"), "data" => $data ];

    }

    public function apiGetEntityRecord($unid)
    {
        $record = $this->model->whereJsonContains('record_values', ['unid' => $unid])->first();
        $record = json_decode($record,JSON_UNESCAPED_UNICODE);
        return ["status" => true, "message" => __("api.success"), "data" => $record ];

    }
}

