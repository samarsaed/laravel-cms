<?php

namespace App\Models\EntityValues\Repositories;

use Illuminate\Http\Request;
 
interface EntityValueRepositoryInterface
{
 public function apiGetEntityRecords($id);
 public function apiGetEntityRecord($unid);

}

