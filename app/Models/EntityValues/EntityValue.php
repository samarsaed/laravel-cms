<?php

namespace App\Models\EntityValues;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Entities\Entity;
class EntityValue extends Model
{
    use SoftDeletes;
    protected $fillable = ['entity_id','unid','record_values'];
    
    public function entity () 
    {
        return $this->belongsTo(Entity::class)->withTrashed();

    }


}


