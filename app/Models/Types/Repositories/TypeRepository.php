<?php

namespace App\Models\Types\Repositories;

use App\Models\Types\Type;
use app\Repositories\BaseRepository;
use App\Models\Types\Repositories\TypeRepositoryInterface;
use App\Models\Types\Repositories\ViewData;

class TypeRepository extends BaseRepository implements TypeRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Type $type)
    {
        $this->model = $type;
    }


}

