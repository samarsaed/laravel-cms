<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','key'];
    const KEY_STRING = 1;
    const KEY_INTEGER = 2;
    const KEY_FLOAT = 3;
    const KEY_BOOLEAN = 4;
    const KEY_FILE = 5;
    const KEY_DATE = 6;
    const KEY_TIME = 7;
    const KEY_TIME_STAMP = 8;
    const KEY_REF = 9;
    const KEYS=[
        self::KEY_STRING,
        self::KEY_INTEGER,
        self::KEY_FLOAT,
        self::KEY_BOOLEAN,
        self::KEY_FILE,
        self::KEY_DATE,
        self::KEY_TIME,
        self::KEY_TIME_STAMP,
        self::KEY_REF
    ];
    

}


