<?php

namespace App\Models\Entities\Repositories;

use Illuminate\Http\Request;
 
interface EntityRepositoryInterface
{
   public function apiAssignAttribute($data);
}

