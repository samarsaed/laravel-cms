<?php

namespace App\Models\Entities\Repositories;

use App\Models\Entities\Entity;
use App\Repositories\BaseRepository;
use App\Models\Entities\Repositories\EntityRepositoryInterface;
use App\Models\Entities\Repositories\ViewData;

class EntityRepository extends BaseRepository implements EntityRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Entity $entity)
    {
        $this->model = $entity;
    }

    public function apiAssignAttribute($data)
    {
       $validates=[];
       $attribute['is_unique'] = $data['is_unique'];
       $attribute['is_required'] = $data['is_required'];    

       if (array_key_exists("min_length", $data)) {
          $attribute['min_length'] = $data['min_length'];
       }
       if (array_key_exists("max_length", $data)) {
        $attribute['max_length'] = $data['max_length'];
     }
     if (array_key_exists("min_value", $data)) {
        $attribute['min_value'] = $data['min_value'];
     }
     if (array_key_exists("max_value", $data)) {
        $attribute['max_value'] = $data['max_value'];
     }
     if (array_key_exists("values", $data)) {
         $attribute['values'] = $data['values'];
     }

     array_push($validates, $attribute);
     $validates = json_encode($validates,JSON_UNESCAPED_UNICODE);
     $entity = $this->model->findOrFail($data['entity_id']);

     $entity->attributes()->attach($data['attribute_id'], ['validations' => $validates]);
     
     return ["status" => true, "message" => __("api.success"), "data" => $this->model->with('attributes')->findOrFail($data['entity_id'])];
    }


}

