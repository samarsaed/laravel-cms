<?php

namespace App\Models\Entities\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEntityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'name' => 'required|string|unique:entities,name,'.request()->route('entity'),
        ];

    }

    public function attributes()
    {
        return [
            'name' => __('api.name'),
        ];
    }
}
