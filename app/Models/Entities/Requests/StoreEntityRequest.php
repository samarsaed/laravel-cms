<?php

namespace App\Models\Entities\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreEntityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {

        return [
            'name' => 'required|string|unique:entities,name',
        ];

    }

    public function attributes()
    {
        return [
            'name' => __('api.name'),
        ];
    }
}
