<?php

namespace App\Models\Entities\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Types\Type;
use App\Models\CustomAttributes\CustomAttribute;
class AssignAttributeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {

        return [
            'attribute_id'=> 'required|exists:custom_attributes,id',
            'entity_id' =>'required|exists:entities,id',
            'is_unique'=>'required|in:1,0',
            'is_required'=>'required|in:1,0',
            'min_length'=>'nullable',
            'max_length'=>'nullable',
            'min_value'=>'nullable',
            'max_value'=>'nullable',
            'values' =>  Rule::requiredIf(function () {
                   $attribute=CustomAttribute::where('id',$this->input('attribute_id'))->first();
                return $attribute ? $attribute->type()->first()->key == Type::KEY_BOOLEAN : false;
            }).'|nullable|array',

        ];

    }

    public function attributes()
    {
        return [
            //'name' => __('api.name'),
        ];
    }
}
