<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CustomAttributes\CustomAttribute;

class Entity extends Model
{
    use SoftDeletes;
    protected $fillable = ['name'];

    public function attributes ()
    {

        return $this->belongsToMany(
            CustomAttribute::class,
            'entities_attributes',
            'entity_id',
            'attribute_id'
        )->withPivot(['validations','id'])
            ->withTimestamps();

    }
    
}


