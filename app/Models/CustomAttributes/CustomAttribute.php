<?php

namespace App\Models\CustomAttributes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Types\Type;
use App\Models\Entities\Entity;

class CustomAttribute extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','type_id','entity_id'];

     public function type () 
    {
        return $this->belongsTo(Type::class)->withTrashed();

    }

     public function entity () 
    {
        return $this->belongsTo(Entity::class)->withTrashed();

    }

    

}


