<?php

namespace App\Models\CustomAttributes\Repositories;

use App\Models\CustomAttributes\CustomAttribute;
use App\Repositories\BaseRepository;
use App\Models\CustomAttributes\Repositories\CustomAttributeRepositoryInterface;
use App\Models\CustomAttributes\Repositories\ViewData;

class CustomAttributeRepository extends BaseRepository implements CustomAttributeRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(CustomAttribute $customattribute)
    {
        $this->model = $customattribute;
    }


}

