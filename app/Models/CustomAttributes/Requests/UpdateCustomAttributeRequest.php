<?php

namespace App\Models\CustomAttributes\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Types\Type;
class UpdateCustomAttributeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'name' => 'required|string',
            'type_id'=> 'required|exists:types,id',
            'entity_id' => Rule::requiredIf(function () {
                return $this->input('type_id') == Type::where('key',Type::KEY_REF)->first()->id;
            }).'|nullable|exists:entities,id',

        ];

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.customattributes.'),
        ];
    }
}
