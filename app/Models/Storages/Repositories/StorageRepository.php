<?php

namespace App\Models\Storages\Repositories;

use App\Models\Storages\Storage;
use App\Repositories\BaseRepository;
use App\Models\Storages\Repositories\StorageRepositoryInterface;
use App\Models\Storages\Repositories\ViewData;

class StorageRepository extends BaseRepository implements StorageRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Storage $storage)
    {
        $this->model = $storage;
    }




}

