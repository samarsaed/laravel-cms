<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admins\Repositories\AdminRepositoryInterface;
use App\Models\Admins\Requests\AdminLoginRequest;
use App\Helpers\ApiResponse;

class AdminController extends Controller
{

    private $adminRepository;
    public $apiResponse;

    public function __construct(AdminRepositoryInterface $adminRepository, ApiResponse $apiResponse)
    {
        $this->adminRepository = $adminRepository;
        $this->apiResponse = $apiResponse;
    }

    public function login(AdminLoginRequest $request)
    {
        $response = $this->adminRepository->apiLogin($request->all());

        return $this->apiResponse->responseHandler($response);

    }

}


