<?php

namespace App\Http\Controllers\Backend\Entity;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Entities\Repositories\EntityRepositoryInterface;
use App\Models\Entities\Requests\StoreEntityRequest;
use App\Models\Entities\Requests\UpdateEntityRequest;
use App\Models\Entities\Requests\AssignAttributeRequest;
use App\Helpers\ApiResponse;

class EntityController extends CustomController
{

    protected $storeRequestFile = StoreEntityRequest::class;

    protected $updateRequestFile = UpdateEntityRequest::class;

    private $entityRepository;
    public $apiResponse;

    public function __construct(EntityRepositoryInterface $entityRepository,ApiResponse $apiResponse)
    {
        parent::__construct($entityRepository);
        $this->entityRepository = $entityRepository;
        $this->apiResponse = $apiResponse;

    }

    public function assignAttribute(AssignAttributeRequest $request)
    {
        $response = $this->entityRepository->apiAssignAttribute($request->all());

        return $this->apiResponse->responseHandler($response);

    }


}


