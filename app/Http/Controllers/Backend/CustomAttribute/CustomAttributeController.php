<?php

namespace App\Http\Controllers\Backend\CustomAttribute;

use App\Http\Controllers\Backend\CustomController;
use App\Models\CustomAttributes\Repositories\CustomAttributeRepositoryInterface;
use App\Models\CustomAttributes\Requests\StoreCustomAttributeRequest;
use App\Models\CustomAttributes\Requests\UpdateCustomAttributeRequest;
use App\Helpers\ApiResponse;

class CustomAttributeController extends CustomController
{

    protected $storeRequestFile = StoreCustomAttributeRequest::class;

    protected $updateRequestFile = UpdateCustomAttributeRequest::class;

    private $attributeRepository;
    public $apiResponse;

    public function __construct(CustomAttributeRepositoryInterface $attributeRepository,ApiResponse $apiResponse)
    {
        parent::__construct($attributeRepository);
        $this->attributeRepository = $attributeRepository;
        $this->apiResponse = $apiResponse;

    }



}


