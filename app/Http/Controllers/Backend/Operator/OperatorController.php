<?php

namespace App\Http\Controllers\Backend\Operator;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Operators\Repositories\OperatorRepositoryInterface;
use App\Models\Operators\Requests\StoreOperatorRequest;
use App\Models\Operators\Requests\UpdateOperatorRequest;
use App\Models\Operators\Requests\OperatorLoginRequest;
use App\Helpers\ApiResponse;

class OperatorController extends CustomController
{

    protected $storeRequestFile = StoreOperatorRequest::class;

    protected $updateRequestFile = UpdateOperatorRequest::class;

    private $operatorRepository;
    public $apiResponse;

    public function __construct(OperatorRepositoryInterface $operatorRepository, ApiResponse $apiResponse)
    {
        parent::__construct($operatorRepository);
        $this->operatorRepository = $operatorRepository;
        $this->apiResponse = $apiResponse;
    }

    public function login(OperatorLoginRequest $request)
    {
        $response = $this->operatorRepository->apiLogin($request->all());

        return $this->apiResponse->responseHandler($response);

    }


}


