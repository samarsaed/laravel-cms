<?php

namespace App\Http\Controllers\Backend\EntityValue;

use App\Http\Controllers\Backend\CustomController;
use App\Models\EntityValues\Repositories\EntityValueRepositoryInterface;
use App\Models\EntityValues\Requests\StoreEntityValueRequest;
use App\Models\EntityValues\Requests\UpdateEntityValueRequest;
use App\Helpers\ApiResponse;

class EntityValueController extends CustomController
{
    protected $storeRequestFile = StoreEntityValueRequest::class;

    protected $updateRequestFile = UpdateEntityValueRequest::class;

    private $entityValueRepository;
    public $apiResponse;

    public function __construct(EntityValueRepositoryInterface $entityValueRepository, ApiResponse $apiResponse)
    {
        parent::__construct($entityValueRepository);
        $this->entityValueRepository = $entityValueRepository;
        $this->apiResponse = $apiResponse;
    }

    public function getEntityRecords($id)
    {
        $response = $this->entityValueRepository->apiGetEntityRecords($id);

        return $this->apiResponse->responseHandler($response);

    }

    public function getEntityRecord($unid)
    {
        $response = $this->entityValueRepository->apiGetEntityRecord($unid);

        return $this->apiResponse->responseHandler($response);

    }

}


