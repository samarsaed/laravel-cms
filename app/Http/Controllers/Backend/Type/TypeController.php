<?php

namespace App\Http\Controllers\Backend\Type;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Types\Repositories\TypeRepository;
use App\Models\Types\Requests\StoreTypeRequest;
use App\Models\Types\Requests\UpdateTypeRequest;


class TypeController extends CustomController
{
    protected $view = 'backend.types';

    protected $route = 'admin.types';

    protected $storeRequestFile = StoreTypeRequest::class;

    protected $updateRequestFile = UpdateTypeRequest::class;

    public function __construct(TypeRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {

        $types = Type::query()->search(request()->get('search'))->latest()->paginate();

        return view('backend.types.index', compact('types'));

    }

}


