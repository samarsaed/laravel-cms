<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Types\Type;
use App\Models\Admins\Admin;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $types=[

            Type::KEY_STRING => 'string',
            Type::KEY_INTEGER => 'integer',
            Type::KEY_FLOAT => 'float',
            Type::KEY_BOOLEAN => 'boolean',
            Type::KEY_FILE => 'file',
            Type::KEY_DATE => 'date',
            Type::KEY_TIME => 'time',
            Type::KEY_TIME_STAMP => 'timestamp',
            Type::KEY_REF => 'reference',
        ];  
        
        foreach ($types as $key => $type) 
        {
             Type::create([
               'name'=>$type,
               'key'=>$key
             ]);
          
        }

        Admin::query()->create([
            'email' => 'admin@cms.com',
            'username'=>'superAdmin',
            'password' => '123456'
        ]);


    }
}
