<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitiesAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('entity_id')
            ->nullable()
            ->references('id')
            ->on('entities')
            ->onDelete('cascade');
            $table->foreignId('attribute_id')
            ->nullable()
            ->references('id')
            ->on('custom_attributes')
            ->onDelete('cascade');
            $table->text('validations')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities_attributes');
    }
}
