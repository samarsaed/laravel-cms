<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admins/', 'namespace' => 'Backend\Admin'], function () {

    Route::post("login", "AdminController@login"); 


});

Route::group(['namespace' => 'Backend\Operator'], function () {

    Route::group(['prefix' => 'operators/'], function () {
        Route::post("login", "OperatorController@login"); 
      });

    Route::group(['middleware' => 'auth:admin_api'], function () {
         Route::resource('operators', 'OperatorController')->except(['create','edit']);
    });


});

Route::group(['namespace' => 'Backend\Entity'], function () {

    Route::group(['middleware' => 'auth:admin_api'], function () {
         Route::resource('entities', 'EntityController')->except(['create','edit']);
         Route::post("assignAttribute", "EntityController@assignAttribute");

    });

});

Route::group(['namespace' => 'Backend\CustomAttribute'], function () {

  Route::group(['middleware' => 'auth:admin_api'], function () {
       Route::resource('customAttributes', 'CustomAttributeController')->except(['create','edit']);
  });


});

Route::group(['namespace' => 'Backend\EntityValue'], function () {

  Route::group(['middleware' => 'auth:operator_api'], function () {
    Route::post('entityValues', 'EntityValueController@store')->name('entityValues.store');
    Route::get('records/{entity}', 'EntityValueController@getEntityRecords');
    Route::get('record/{unid}', 'EntityValueController@getEntityRecord');
});


});